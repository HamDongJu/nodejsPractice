var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');   

var app = express();


// app.use((req, res) => {
//     //method chaining
//     // res.status(200).json({name : 'thomas', region : 'incheon'});
//      //url에서 값 추출
//      res.status(200).send(req.query.name+ " : " +req.query.region);
// });

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));

// post 방식
app.post('/login', (req, res) => {
    res.send(`${req.body.login} : ${req.body.password}`);
});

// cookie를 만들고 출력
app.get('/getCookie', (req, res) => {
    res.send(req.cookies);
});
app.get('/setCookie', (req, res) => {
    res.cookie('stringKey', 'cookie');
    res.cookie('jsonKey', {name : 'thomas', region : 'incheon'});

    res.redirect('/getCookie');
});


// 라우팅
app.get('/a', (req, res) => {
    res.status(200).send('a page');
});

// 파라미터 
app.get('/user/:id', (req, res) => {
    res.status(200).send(req.params.id);
});





app.listen(3000, () => {
    console.log("server running");
});
var express = require('express');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var app = express();

app.use(session({
    secret : 'secret key',
    resave : false,
    saveUninitialized : true
}));

app.get('/', (req, res) => {
    req.session['now'] = (new Date()).toUTCString();
    res.send(req.session);
});


app.listen(3000, () => {
    console.log("server running");
});
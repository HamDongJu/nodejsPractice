var express = require('express');
var bodyParser = require('body-parser');
var sql = require('mssql');

var config = {
    server: '192.168.78.1',
    database: 'Chinook',
    user: 'sa',
    password: '0000',
    port: 1433
};

var app = express();

app.use(bodyParser.urlencoded({
    extended: false
}));

//select
app.get('/user', (req, res) => {
    var pool = new sql.ConnectionPool(config, err => {
        var request = pool.request();

        request.execute('Artist_SelectAll',
            (err, result, returnValue) => {
                res.status(200).json(result.recordset);
            });
    });
});

// select
app.get('/user/:id', (req, res) => {
    var pool = new sql.ConnectionPool(config, err => {
        var request = pool.request();

        var id = req.params.id;

        request.input('ArtistId', sql.Int, id);

        request.execute('Artist_SelectById',
            (err, result, returnValue) => {
                if (result.rowsAffected == 0)
                    res.status(404).json({
                        Error: `id ${id} does not exist,`
                    });
                else
                    res.status(200).json(result.recordset);
            });
    });
});

// insert
app.post('/user', (req, res) => {
    var pool = new sql.ConnectionPool(config, err => {
        var request = pool.request();

        var name = req.body.name;
        request.input('Name', sql.NVarChar, name);

        request.execute('Artist_Insert',
            (err, result, returnValue) => {
                if (result.rowsAffected == 0)
                    res.status(500).json({
                        Error: `failed to insert ${name}`
                    });
                else
                    res.status(200).json(result.recordset);
            });
    });
})

//update
app.put('/user/:id', (req, res) => {
    var pool = new sql.ConnectionPool(config, err => {
        var request = pool.request();

        var id = req.params.id;
        var name = req.body.name;

        request.input('ArtistId', sql.Int, id);
        request.input('Name', sql.NVarChar, name);

        request.execute('Artist_Update',
            (err, result, returnValue) => {
                if (result.rowsAffected == 0)
                    res.status(500).json({
                        Error: `failed to update ${name}`
                    });
                else
                    res.status(200).json(result.recordset);
            });
    });
})

//delete 
app.delete('/user/:id', (req, res) => {
    var pool = new sql.ConnectionPool(config, err => {
        var request = pool.request();

        var id = req.params.id;

        request.input('ArtistId', sql.Int, id);

        request.execute('Artist_Delete',
            (err, result, returnValue) => {
                if (result.rowsAffected == 0)
                    res.status(500).json({
                        Error: `failed to delete ${id}`
                    });
                else
                    res.status(200).json(result.recordset);
            });
    });
})

app.listen(3000, () => {
    console.log('listening on port 3000')
});
var express = require('express');
var bodyParser = require('body-parser');
var sql = require('mssql');
var DummyDB = require('./db.js');

var db = DummyDB();
var app = express();

var config = {
    server : '192.168.78.1',
    database : 'Chinook',
    user : 'sa',
    password : '0000',
    port : 1433
};

app.use(bodyParser.urlencoded({
    extended : false
}));

app.get('/user', (req, res) => {
    res.status(200).send(db.get());
});

app.put('/user/:id', (req, res) => {
    var id = req.params.id;
    var name = req.body.name;
    var region = req.body.region;

    var user = db.get(id);
    user.name = name;

    res.send(user);
});

app.delete('/user/:id', (req, res) => {
    res.send(db.delete(req.params.id));
});

// insert
app.post('/user', (req, res) => {
        var pool1 = new sql.ConnectionPool(config, err => {
        var request = pool1.request();
        var name = req.body.name;
        request.input('Name', sql.NVarChar, name);
        request.execute('Artist_Insert', 
                    (err, result, returnValue) => {
                        // 영향을 받은 로우 수 : rowsAffected
                        if(result.rowsAffected==0)
                            res.status(500).json({error:`failed to insert ${name}`});
                        else
                            res.status(200).json(result.recordset);  
                    });
    });
});

// select
app.get('/user/:id', (req, res) => {
        var pool1 = new sql.ConnectionPool(config, err => {
        var request = pool1.request();
        var id = req.params.id;
        request.input('artistid', sql.Numeric, id);
        request.execute('Artist_SelectByArtistId', 
                    (err, result, returnValue) => {
                        // 영향을 받은 로우 수 : rowsAffected
                        if(result.rowsAffected==0)
                            res.status(500).json({error:`does not exist ${id}`});
                        else
                            res.status(200).json(result.recordset);  
                    });
    });
});

app.get('/user', (req, res) => {
        var pool1 = new sql.ConnectionPool(config, err => {
        var request = pool1.request();
        request.execute('Artist_SelectAll', 
                    (err, result, returnValue) => {
                        // 영향을 받은 로우 수 : rowsAffected
                        if(result.rowsAffected==0)
                            res.status(500).json({error:`does not exist`});
                        else
                            res.status(200).json(result.recordset);  
                    });
    });
});

app.listen(3000, () => {
    console.log("server running");
});



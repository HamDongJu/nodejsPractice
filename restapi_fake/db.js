module.exports = function() {
    var DummyDB = {};
    var storage = [];
    var count = 0;

    // constructor
    DummyDB.get = function(id) {
        id = (typeof id == 'string') ? Number(id) : id;

        if(id) {
            var index = storage.findIndex(x => x.id == id);

            if (index == -1)
                return null;
            
            return storage[index];
        }
        else {
            return storage;
        }
    };

    DummyDB.insert = function(data) {
        data.id = ++count;
        storage.push(data);
        return data;
    };

    DummyDB.delete = function(id) {
        id = (typeof id  == 'string') ? Number(id) : id;

        var index = storage.findIndex(x => x.id == id);
        
        if (index == -1) {
            return false;
        }
        
        storage.splice(index, 1);
        return true;
    };
    return DummyDB;
};

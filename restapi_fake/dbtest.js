var fakeDB = require('./db.js');

var db = fakeDB();

db.insert({name : 'thomas'});
db.insert({name : 'friend'});

console.log(db.get(1));

db.delete(1);

console.log(db.get());